(function($){

    var methods = {
        init: function(params) {
            var options = $.extend({}, params),
                el = $(this),
                slides = el.find('.slides li'),
                max = slides.size() - 1,
                setActive = function(){
                    slides.eq(0).addClass('active');
                },
                navAdd = function(){
                    var elementCont = '<p class="nav-elements"></p>',
                        prev = document.createElement('a'),
                        next = document.createElement('a');

                    console.log(index());
                    $(prev)
                        .addClass('prev')
                        .append('<i class="fa fa-angle-left"></i>')
                        .click(function(){
                           var prevEl = getPrev();
                           slide(prevEl);
                        });
                    $(next)
                        .addClass('next')
                        .append('<i class="fa fa-angle-right"></i>')
                        .click(function(){
                            var nextEl = getNext();
                            slide(nextEl);
                        });

                    el.append(elementCont);
                    el.find('.nav-elements')
                        .append(prev)
                        .append(next);
                },
                getPrev = function(){
                    var prev = index();

                    if(prev = 0){
                        prev = max - 1;
                    }else{
                        prev--;
                    };

                    return prev;
                },
                getNext = function(){
                    var next = index();

                    if(next = max-1){
                        next = 0;
                    }else{
                        next++;
                    };

                    return next;
                },
                getN = function(n){
                    if (n > max) n = 0;
                    if(n < 0) n = max;

                    return slides.eq(n);
                },
                getA = function(){
                    var active = el.find('.slides li.active') || getN(0);

                    return active;
                },
                index = function(){
                   var i = 0,
                       ind = 0;
                   while(i < max){
                       if (getN(i).hasClass('active')){
                           ind = i;
                           i = max;
                       };
                       i++;
                   };

                    return ind;
                },
                slide = function(n){
                    var now = getA(),
                        next = getN(n);

                    next.css({'z-index': 2});
                    now.fadeOut('slow');
                    now.addClass('active');
                    next.removeClass('active');
                    slides.removeAttr('style');
                };

            setActive();
            navAdd();
        }
    };

    $.fn.feature = function(method){

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод "' +  method + '" не найден в плагине jQuery.feature' );
        }
    };

})(jQuery);